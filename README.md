# Inlets

This repository contains all the resources needed to deploy an [inlets](https://github.com/alexellis/inlets) exit node into a free namespace at [k8spin.cloud](https://k8spin.cloud)

## Let's deploy it

Having a [K8Spin.cloud](https://k8spin.cloud) kubernetes configuration file named `inlets.config` inside a temporal directory `/tmp/tmp.HHnDXYthMr`:

```bash
$ export KUBECONFIG=/tmp/tmp.HHnDXYthMr/inlets.config
```

Create a kubernetes secret with a token to run [inlets](https://github.com/alexellis/inlets) securely:

```bash
$ kubectl create secret generic inlets --from-literal=TOKEN=PUT_YOUR_SECURE_TOKEN_HERE
```

Now, change the [deploy/ingress.yml](deploy/ingress.yml) file. Modify the `certmanager.k8s.io/issuer` annotation to your issuer name. You can find it running:

```bash
$ kubectl get issuer
NAME                            AGE
angelbarrerasanchez-gmail-com   30m
```

Open the [k8spin]((https://console.k8spin.cloud)) console, look for the assigned `ingress domain`, it should be something like: `*.angelbarrerasanchez.apps.k8spin.cloud`.

Then, choose a hostname to create the `ingress` resource: `inlets.angelbarrerasanchez.apps.k8spin.cloud` will works.

Finally, modify `hosts` attributes (`tls`)(`rules`) in the [deploy/ingress.yml](deploy/ingress.yml) file.

Your `ingress` should look like:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: inlets
  annotations:
    ingress.kubernetes.io/ssl-redirect: "true"
    cert-manager.io/issuer: angelbarrerasanchez-gmail-com
spec:
  tls:
  - hosts:
    - inlets.angelbarrerasanchez.apps.k8spin.cloud
    secretName: inlets-certificate
  rules:
  - host: inlets.angelbarrerasanchez.apps.k8spin.cloud
    http:
      paths:
      - path: /
        backend:
          serviceName: inlets
          servicePort: 80
```

Now, we are ready to deploy the exit-node:

```bash
$ pwd
/tmp/tmp.HHnDXYthMr/inlets
$ kubectl apply -R -f deploy/
deployment.apps/inlets created
ingress.extensions/inlets created
service/inlets created
```

After a while, the inlet's exit node will be ready.

## Use it!

Run a simple service in your local machine. For example, run [Python's built-in HTTP server](https://docs.python.org/2/library/simplehttpserver.html):

```bash
$ mkdir -p /tmp/inlets-test/
$ cd /tmp/inlets-test/
$ echo "# Hello inlets at k8spin.cloud" > README.md
$ python -m SimpleHTTPServer 3000
Serving HTTP on 0.0.0.0 port 3000 ...
```

![localhost](assets/localhost.png)

Then install [inlets](https://github.com/alexellis/inlets):

```bash
# Install to local directory
$ curl -sLS https://get.inlets.dev | sh
# Install to /usr/local/bin/
$ curl -sLS https://get.inlets.dev | sudo sh
```

And start the [inlets](https://github.com/alexellis/inlets) client:

```bash
$ inlets client --remote="wss://inlets.angelbarrerasanchez.apps.k8spin.cloud" --upstream="localhost:3000" --token=PUT_YOUR_SECURE_TOKEN_HERE --print-token=false
2019/08/07 12:25:46 Upstream:  => localhost:3000
map[X-Inlets-Id:[1df2ffd28f85463085e86a7068dc0ca9] X-Inlets-Upstream:[=localhost:3000] Authorization:[Bearer -.-]]
INFO[0000] Connecting to proxy                           url="wss://inlets.angelbarrerasanchez.apps.k8spin.cloud/tunnel"
```

Let's dig into the `inlets client` command flags:

- `--remote` Points to the ingress hostname we choose.
- `--upstream` Local service will be exposed through the exit node. In this example, python web server created before.
- `--token` Secret token created before.

Then visit: [https://inlets.angelbarrerasanchez.apps.k8spin.cloud](https://inlets.angelbarrerasanchez.apps.k8spin.cloud) and you will see your local server through an exit node on top of [k8spin]((https://k8spin.cloud)) namespace.

![Remote](assets/remote.png)
